function Gallery(params) {
    let el = null, ul = null, largImg = null;
    let loadedImgs = false;
    let {items, value:numOfImg} = params;

    function createElements() {
        el = document.createElement('div');
        ul = document.createElement('ul');
        ul.setAttribute('id', 'thumbs');

        ul.innerHTML = items.map(function (item) {
            return `<li><img src="${item.thumbs}" data-img="${item.original}"></li>`;
        }).join('');

        el.innerHTML = `<p><img id="largeImg" src="" alt=""></p>`;
        el.append(ul);
        largImg = el.querySelector('#largeImg');
    }

    function predloderIMGS(items) {
        items.forEach(function (item) {
            const img = document.createElement('img');
            img.src = item.original;
        });
    }

    function render() {
        if (!items.length) return 'No items to render';

        if (!el) createElements();

        if (!loadedImgs) predloderIMGS(items);
        loadedImgs = true;

        largImg.src = items[numOfImg].original;
        ul.onclick = handleClick;

        return el;
    }

    function handleClick(e) {
        const thumb = e.target;
        if (thumb.tagName == "IMG") {
            largImg.src = thumb.dataset.img;
        }
    }

    function changeLargeIMG(num) {
        numOfImg = num;
        render();
    }

    this.render = render;
    this.changeLargeIMG = changeLargeIMG;
}
